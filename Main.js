'use strict';
function showSplashScreen() {
    document.getElementById("page-splash").style.display = "flex";
}

function hideSplashScreen() {
    document.getElementById("page-splash").style.display = 'none';
}
//--<Создано для проверки createCommentElement()
//Создайте объект пользователя
function User(id, name, surname, login, email, password, isAuthorized) {
    this.id = id;
    this.name = {
        name: name,
        surname: surname
    };
    this.login = login;
    this.email = email;
    this.password = password;
    this.isAuthorized = isAuthorized;               //Как понять, авторизован он или нет
}
//Создаю объект лайк
function Like(id, user, datetime) {
    this.id = id;
    this.user = user;
    this.datetime = datetime;
}
//Создайте объект поста.
function Post(id, imgLink, description, datetime, user) {
    this.id = id;
    this.imgLink = imgLink;
    this.description = description;
    this.datetime = datetime;
    this.user = user;           //Как вы свяжете его с пользователем? - Попробовал связать по объекту, получилась связь. Не уверен в корректности, но работает
    this.like = undefined;
}

//Создайте объект комментария
function Comment(id, text, datetime, post, user) {
    this.id = id;
    this.text = text;
    this.datetime = datetime;
    this.post = post;               //Как бы вы связали его с постом и пользователем? - ну как-то так
    this.user = user;               //Как бы вы связали его с постом и пользователем? - ну как-то так
}

let userNames = ["Kamil", "Ulia", "Milen", "Amal", "Nael"];
let userSurnames = ["Mirzhalalov", "Potapova", "Mirzhalalov", "Arapbaev", "Vaitkus"];
let userLogins = ["kempl95kg", "potapovaUlia", "antAras", "arapbaev88", "ntw98"];

//Создал массив пользователей
let users = [];
function createUsers(){
    for (let i = 0; i< userNames.length; i++){
        users.push(new User(i, userNames[i], userSurnames[i], userLogins[i], userLogins[i].concat("@gmail.com"), i+i+i, false));
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}
//Создайте массив постов
let postQty = 10;
let posts = [];
function createPosts(){
    for (let i = 0; i< postQty; i++){
        posts.push(new Post(i, "http://placekitten.com/500/500","description".concat(i.toString()), new Date(), users[getRandomInt(0,users.length)]));
    }
}
//Добавление новый пост в массив
function addNewPost(postArray, usersArray){
    postArray.push(new Post(postArray.length, "description".concat(postArray.length), new Date(), usersArray[getRandomInt(0,usersArray.length)]));
}

function authorizeUser(User){
    User.isAuthorized = true;
}
//Добавление лайка посту
function addLikeToPost(postId){
    for (let i = 0; i < posts.length; i++){
        if (posts[i].id === postId){
            posts[i].like = new Like("1", users[getRandomInt(0,users.length)], new Date());
        }
    }
}
//Удаление лайка у поста
function delLikeFromPost(postId){
    for (let i = 0; i < posts.length; i++){
        if (posts[i].id === postId){
            posts[i].like = undefined;
        }
    }
}

//Создаю объекты, выполняю вышесозданные методы:
createUsers();
createPosts();
let someUser = users[2];                                                                                    //Создайте объект пользователя.
let somePost = new Post("1", "http://placekitten.com/500/500","blablabla", new Date(), users[0]);                              //Создайте объект поста.
let someComment = new Comment("1", "textttt", "this is a comment", somePost, someUser);    //Создайте объект комментарий.
//-->Создано для проверки createCommentElement()
//Комментарий
function createCommentElement(comment) {
    let commentElement = document.createElement('div');
    commentElement.className = "py-2 pl-3";
    let userLink = document.createElement('a');
    let commentText = document.createElement('p');
    userLink.className = "muted";
    userLink.href = "/users/" + comment.user.id.toString();
    userLink.innerHTML = comment.user.login;
    commentText.innerHTML = comment.text;

    //Соединяю
    commentElement.append(userLink);
    userLink.after(commentText);
    return commentElement;
}

function addCommentElement(commentElement){
    document.getElementsByClassName('comments')[0].append(commentElement);
}
function addCommentElementToNewPost(commentElement, postElement){
    postElement.getElementsByClassName('comments')[0].append(commentElement);
}
//Пост
function createPostElement(post) {
    let postElement = document.createElement('div');
    postElement.className = "card my-3";
    postElement.innerHTML =
        '<!-- image block start -->\n' +
        '                    <div>\n' +
        `                        <img class="d-block w-100" src="${post.imgLink}" alt="Post image">\n` +
        '                    </div>\n' +
        '                    <!-- image block end -->\n' +
        '                    <div class="px-4 py-3">\n' +
        '\n' +
        '                        <!-- post reactions block start -->\n' +
        '                        <div class="d-flex justify-content-around">\n' +
        '                            <span class="h1 mx-2 text-danger">\n' +
        '                                <i class="fas fa-heart"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted">\n' +
        '                                <i class="far fa-heart"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted">\n' +
        '                                <i class="far fa-comment"></i>\n' +
        '                            </span>\n' +
        '                            <span class="mx-auto"></span>\n' +
        '                            <span class="h1 mx-2 muted">\n' +
        '                                <i class="far fa-bookmark"></i>\n' +
        '                            </span>\n' +
        '                            <span class="h1 mx-2 muted">\n' +
        '                                <i class="fas fa-bookmark"></i>\n' +
        '                            </span>\n' +
        '                        </div>\n' +
        '                        <!-- post reactions block end -->\n' +
        '                        <hr>\n' +
        '                        <!-- post section start -->\n' +
        '                        <div>\n' +
        '                            <p class="post-description"></p>\n' +
        '                        </div>\n' +
        '                        <!-- post section end -->\n' +
        '                        <hr>\n' +
        '                        <!-- comments section start -->\n' +
        '                        <div class="comments">\n' +
        '                        </div>\n' +
        '                        <!-- comments section end -->\n' +
        '                    </div>'
    ;
    postElement.getElementsByClassName("post-description")[0].textContent = post.description;
    //здесь я должен пройти по выборке всех комментариев, найти комментарии, относящиеся к переданному post, и циклом выполнить addComment
    //не сложно, но дольше. Просто добавлю так:
    for (let i = 0; i<3; i++){
        addCommentElementToNewPost(createCommentElement(someComment), postElement);
    }
    return postElement;
}

function addPost(postElement) {
    document.getElementsByClassName("col col-lg-7 posts-container")[0].append(postElement);
}
//Проверка
//showSplashScreen();
hideSplashScreen();
addCommentElement(createCommentElement(someComment));
addPost(createPostElement(somePost));

